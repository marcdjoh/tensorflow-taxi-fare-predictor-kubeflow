import kfp
import kfp.dsl as dsl
from kubernetes.client.models import V1EnvVar, V1EnvVarSource, V1SecretKeySelector

MODEL_NAME = 'taxi_fare_predictor_kf'

def send_predictions_op(project_id: str, model_name: str, input_drifted: str):
    return dsl.ContainerOp(
        name='send-predictions',
        image='gcr.io/{}/taxi-fare-utils:latest'.format(project_id),
        command=['python3', '-m', 'prediction_requestor'],
        arguments=[
           '--model_name', model_name,
           '--input_drifted', input_drifted 
        ]
    )

@dsl.pipeline(
    name='Taxi fare drifted predictor',
    description='This pipeline sends drifted predictions to the taxi fare predictor model and stores prediction results'
)
def pipeline(project_id):
    project_env = V1EnvVar(name='GCP_PROJECT', value=project_id)
    send_predictions_task = send_predictions_op(project_id, MODEL_NAME, 1)
    user_name = V1EnvVar(
                name="USER_NAME",
                value_from=V1EnvVarSource(
                    secret_key_ref=V1SecretKeySelector(
                        name="pg-secret", key="user_name", optional=False
                    )
                ),
            )
    pwd = V1EnvVar(
                name="PWD",
                value_from=V1EnvVarSource(
                    secret_key_ref=V1SecretKeySelector(
                        name="pg-secret", key="pwd", optional=False
                    )
                ),
            )
    send_predictions_task.container.add_env_variable(project_env)
    send_predictions_task.container.add_env_variable(user_name)
    send_predictions_task.container.add_env_variable(pwd)
    # Disable caching
    send_predictions_task.execution_options.caching_strategy.max_cache_staleness = "P0D"

if __name__ == '__main__':
    kfp.compiler.Compiler().compile(pipeline, __file__ + '.yaml')
