from datetime import datetime

import kfp
import kfp.dsl as dsl
from kubernetes.client.models import V1EnvVar, V1EnvVarSource, V1SecretKeySelector

MODEL_NAME = 'taxi_fare_predictor_kf'

def is_retraining_needed_op(project_id: str, model_name: str, acceptable_gap: str):
    return dsl.ContainerOp(
        name='is-retraining-needed',
        image='gcr.io/{}/taxi-fare-utils:latest'.format(project_id),
        command=['python3', '-m', 'is_retraining_needed'],
        arguments=[
            '--model_name', model_name,
            '--acceptable_gap', acceptable_gap
        ],
        file_outputs={'output': '/tmp/output'}
    )

def trigger_model_training_pipeline_op(project_id: str, bucket_name: str, train_steps: str, model_folder: str, 
    host: str):
    return dsl.ContainerOp(
        name='trigger-model-training',
        image='gcr.io/{}/taxi-fare-utils:latest'.format(project_id),
        command=['python3', '-m', 'trigger_pipeline'],
        arguments= [
            '--host', host,
            '--project_id', project_id,
            '--bucket_name', bucket_name,
            '--train_steps', train_steps,
            '--model_folder', model_folder
        ]
    )

@dsl.pipeline(
    name='retraining-checker',
    description='This pipeline checks if the taxi fare predictor model should already been retrained'
)
def pipeline(project_id, bucket_name, train_steps, host, acceptable_gap):
    is_retraining_needed_task = is_retraining_needed_op(project_id, MODEL_NAME, acceptable_gap)
    project_env = V1EnvVar(name='GCP_PROJECT', value=project_id)
    user_name = V1EnvVar(
                name="USER_NAME",
                value_from=V1EnvVarSource(
                    secret_key_ref=V1SecretKeySelector(
                        name="pg-secret", key="user_name", optional=False
                    )
                ),
            )
    pwd = V1EnvVar(
                name="PWD",
                value_from=V1EnvVarSource(
                    secret_key_ref=V1SecretKeySelector(
                        name="pg-secret", key="pwd", optional=False
                    )
                ),
            )
    
    is_retraining_needed_task.container.add_env_variable(project_env)
    is_retraining_needed_task.container.add_env_variable(user_name)
    is_retraining_needed_task.container.add_env_variable(pwd)

    with dsl.Condition(is_retraining_needed_task.output == 'yes'):
        model_folder = datetime.utcnow().isoformat().replace(':', '').replace('-', '').split('.')[0]
        trigger_model_training_pipeline_task = \
            trigger_model_training_pipeline_op(project_id, bucket_name, train_steps, model_folder, host)

    # Disable caching
    is_retraining_needed_task.execution_options.caching_strategy.max_cache_staleness = "P0D"


if __name__ == '__main__':
    kfp.compiler.Compiler().compile(pipeline, __file__ + '.yaml')

