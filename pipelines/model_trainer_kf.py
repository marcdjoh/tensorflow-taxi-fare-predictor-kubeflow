#!/usr/bin/env python3

import os
import json
from datetime import datetime

import kfp
import kfp.dsl as dsl
import kfp.components as comp
from kfp.components import InputPath, OutputPath
from kfp.dsl.types import GCSPath
from typing import NamedTuple
from kubernetes.client.models import V1EnvVar, V1EnvVarSource, V1SecretKeySelector

PYTHON_MODULE = 'trainer.task'
REGION = 'europe-west1'
PYTHON_VERSION = '3.7'
RUNTIME_VERSION ='1.14'
MODEL_NAME = 'taxi_fare_predictor_kf'
MODEL_FOLDER = datetime.utcnow().isoformat().replace(':', '').replace('-', '').split('.')[0]
WAIT_INTERVAL = '30'
TRAINING_TABLE = 'training_jobs'

ML_ENGINE_TRAIN_OP = comp.load_component_from_url(
    'https://raw.githubusercontent.com/kubeflow/pipelines/1.1.1-beta.1/components/gcp/ml_engine/train/component.yaml')
ML_ENGINE_DEPLOY_OP = comp.load_component_from_url(
    'https://raw.githubusercontent.com/kubeflow/pipelines/1.1.1-beta.1/components/gcp/ml_engine/deploy/component.yaml')
CHECK_DEPLOY_OP = comp.load_component_from_file('./deploy_checker_component.yaml')

def get_model_location(bucket_name: str, model_folder: str) -> NamedTuple('returns', [('location', 'GCSPath')]):
    from google.cloud import storage

    folder = 'model/{}/export/exporter/'.format(model_folder)
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blobs = bucket.list_blobs(prefix=folder)
    blob_name = None
    for blob in blobs:
        tmp = len(blob.name.rstrip('/').split('/'))
        if tmp == 5:
            blob_name = blob.name
    print('Model location: {}'.format(blob_name))
    print('Model full location: gs://{0}/{1}'.format(bucket_name, blob_name))

    from collections import namedtuple
    output = namedtuple('returns', ['location'])
    return output('gs://{0}/{1}'.format(bucket_name, blob_name))

GET_MODEL_LOCATION_OP = comp.create_component_from_func(func=get_model_location,
        packages_to_install=['google-cloud-storage==1.26.0'])

def download_blob(source_bucket_name: str, source_blob_name: str, dest_file_path: OutputPath()):
    from google.cloud import storage

    storage_client = storage.Client()

    bucket = storage_client.bucket(source_bucket_name)
    blob = bucket.blob(source_blob_name)
    blob.download_to_filename(dest_file_path)

DOWNLOAD_BLOB_OP = comp.create_component_from_func(func=download_blob,
        packages_to_install=['google-cloud-storage==1.26.0'])


def store_training_job_metrics_op(project_id: str, metrics_file_path: dsl.InputArgumentPath, model_name: str, model_folder: str):
    return dsl.ContainerOp(
        name='store-training-job-metrics',
        image='gcr.io/{}/taxi-fare-utils:latest'.format(project_id),
        command=['python3', '-m', 'metrics_writer'],
        arguments=[
            '--metrics_file_path', metrics_file_path,
            '--model_name', model_name,
            '--model_folder', model_folder
        ]
    )



""" def check_deploy_op(project_id: str, model_name: str, model_folder: str, output_dest: str):
    return dsl.ContainerOp(
        name='check-deploy',
        image='gcr.io/{}/taxi-fare-utils:latest'.format(project_id),
        command=['python3', '-m', 'deploy_checker'],
        arguments=[
           '--project_id', project_id,
           '--model_name', model_name,
           '--model_folder', model_folder,
           '--output_dest', output_dest
        ],
        file_outputs={'output_dest': output_dest}
    ) """
    

@dsl.pipeline(
    name='CloudML training pipeline',
    description='CloudML training pipeline'
)
def pipeline(
    project_id,
    bucket_name,
    train_steps,
    model_folder=MODEL_FOLDER):

    output_gcs_path = 'gs://{}/model/{}'.format(bucket_name, model_folder)
    job_dir = output_gcs_path
    args=json.dumps([
        '--train_data_paths=gs://{}/data/taxi-train*'.format(bucket_name),
        '--eval_data_paths=gs://{}/data/taxi-valid*'.format(bucket_name),
        '--output_dir={}'.format(output_gcs_path),
        '--train_steps={}'.format(train_steps)
    ])
    trainer_gcs_path = 'gs://{}/source/taxifare-0.1.tar.gz'.format(bucket_name)
    package_uris = json.dumps([trainer_gcs_path])
    job_id = '{}_{}'.format(MODEL_NAME, model_folder)

    ml_engine_train_task = ML_ENGINE_TRAIN_OP(
        project_id=project_id, 
        python_module=PYTHON_MODULE, 
        package_uris=package_uris, 
        region=REGION, 
        args=args, 
        job_dir=job_dir, 
        python_version=PYTHON_VERSION,
        runtime_version=RUNTIME_VERSION,
        job_id=job_id,
        wait_interval=WAIT_INTERVAL)

    get_model_location_task = GET_MODEL_LOCATION_OP(bucket_name, model_folder)

    download_blob_task = DOWNLOAD_BLOB_OP(bucket_name, 'tmp/metrics.csv')

    project_env = V1EnvVar(name='GCP_PROJECT', value=project_id)
    user_name = V1EnvVar(
                name="USER_NAME",
                value_from=V1EnvVarSource(
                    secret_key_ref=V1SecretKeySelector(
                        name="pg-secret", key="user_name", optional=False
                    )
                ),
            )
    pwd = V1EnvVar(
                name="PWD",
                value_from=V1EnvVarSource(
                    secret_key_ref=V1SecretKeySelector(
                        name="pg-secret", key="pwd", optional=False
                    )
                ),
            )

    store_training_job_metrics_task = store_training_job_metrics_op(
        project_id,
        dsl.InputArgumentPath(download_blob_task.output),
        MODEL_NAME,
        model_folder)
    store_training_job_metrics_task.container.add_env_variable(user_name)
    store_training_job_metrics_task.container.add_env_variable(pwd)
    
    check_deploy_task = CHECK_DEPLOY_OP(project_id, MODEL_NAME, model_folder)
    
    check_deploy_task.container.add_env_variable(project_env)
    check_deploy_task.container.add_env_variable(user_name)
    check_deploy_task.container.add_env_variable(pwd)

    with dsl.Condition(check_deploy_task.outputs['dest'] == 'relevant'):
        ml_engine_deploy_task = ML_ENGINE_DEPLOY_OP(
        model_uri=get_model_location_task.outputs['location'], 
        project_id=project_id, 
        model_id=MODEL_NAME, 
        version_id='version_{}'.format(MODEL_FOLDER),
        runtime_version=RUNTIME_VERSION, 
        python_version='3.5', 
        set_default=True, 
        wait_interval=WAIT_INTERVAL)

    # Define dependencies
    get_model_location_task.after(ml_engine_train_task)
    download_blob_task.after(ml_engine_train_task)
    store_training_job_metrics_task.after(download_blob_task)
    check_deploy_task.after(get_model_location_task)
    check_deploy_task.after(store_training_job_metrics_task)
    ml_engine_deploy_task.after(check_deploy_task)
    # Disable caching
    download_blob_task.execution_options.caching_strategy.max_cache_staleness = "P0D"
    

if __name__ == '__main__':
    kfp.compiler.Compiler().compile(pipeline, __file__ + '.yaml')