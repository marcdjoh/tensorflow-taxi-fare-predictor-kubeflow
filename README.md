# tensorflow-taxi-fare-predictor-kubeflow
This project demonstrates how to enable automatic tensorflow machine learning model retraining using
Kubeflow pipelines. The retraining strategy relies on a comparison between the training dataset target mean value
and the predictions mean value.
A retraining is automatically triggered by a Kubeflow pipeline if the difference between those two values is
high.


# Technologies
- Kubeflow Pipelines
- Google Cloud SQL / PostgreSQL
- Google Cloud Platform
- AI Platform
- Tensorflow



## Clone the repository
- Using https
```
git clone https://gitlab.com/marcdjoh/tensorflow-taxi-fare-predictor.git
```

- Using ssh
```
git clone git@gitlab.com:marcdjoh/tensorflow-taxi-fare-predictor.git
```

## Create a cloud bucket and copy the training data and the model source code
You will need a Google Cloud Storage bucket to hold the training data, the model source code and the
training outputs. 
```
gsutil mb -l EUROPE-WEST1 gs://<bucket_name>
cd tensorflow-taxi-fare-predictor-kubeflow
gsutil cp -r data/ gs://<bucket_name>/data/
gsutil cp -r taxifare/dist/taxifare-0.1.tar.gz gs://<bucket_name>/source/
```

## Create a Cloud SQL Postgres instance
```
gcloud sql instances create <instance_name> --tier db-f1-micro --database-version=POSTGRES_12 --zone=europe-west1-b
```

## Create a user for the Postgres instance
Use the GCP console

## Create a database in the postgres instance and name it 'retraining-db'
```
gcloud sql databases create retraining-db --instance=<instance_name>
```

## Create two tables in the 'retraining-db': 'training_jobs' and 'predictions'
```
CREATE TABLE training_jobs (
    jobName varchar(200) NOT NULL,
    versionName varchar(200) NOT NULL,
    evalLoss numeric NOT NULL,
    labelMean numeric NOT NULL);

CREATE TABLE predictions (
versionName varchar(200) NOT NULL,
pickuplon numeric NOT NULL,
pickuplat numeric NOT NULL,
dropofflat numeric NOT NULL,
dropofflon numeric NOT NULL,
passengers integer NOT NULL,
prediction numeric NOT NULL);
 ```

## Create a kubeflow deployment on GCP via AI Platform
Use the GCP console

## Create a service to expose a Cloud SQL proxy to your Cloud instance
If Kubeflow is installed on GCP:
```
gcloud container clusters get-credentials <kubeflow_cluster_name> --zone <kubeflow_cluster_zone>
```
```
kubectl apply -f manifests/namespace.yaml
kubectl apply -f manifests/pod.yaml
kubectl apply -f service/yaml
```

## Create a secret using the Postgres user created above credentials
Create a kubernetes secret yaml manifest using the user name and password created for the Postgres user
Place the file under the 'manifests' directory
```
kubectl apply -f manifests/secret.yaml
```

## Build a docker image using the code inside the 'taxi_fare_utils' folder.
```
cd taxi_fare_utils
docker image build -t taxi-fare-utils:latest .
docker image tag taxi-fare-utils:latest gcr.io/<project_id>/taxi-fare-utils:latest
```

## Upload the image in the GCP container registry of the kubeflow project
```
gcloud auth configure-docker
docker image push gcr.io/<project_id>/taxi-fare-utils:latest
```

## Compile the pipelines and upload them using Kubeflow Pipelines UI
### Compile and upload the 'model_trainer_kf' pipeline
```
cd ../pipelines
python model_trainer_kf.py
```

## Activate the AI Platform API
```
gcloud services enable ml.googleapis.com
```

### Compile and upload the 'prediction_requestor_kf' pipeline.
```
python prediction_requestor_kf.py
```
Create a recurrent run on every 5 min and wait for about 1h
Then disable the recurrent run

### Compile and upload the 'drifted_prediction_requestor_kf' pipeline.
```
python drifted_prediction_requestor_kf.py
```
Create a recurrent run on every 5 min

### Compile and upload the 'retraining_checker_kf' pipeline.
Create a recurrent run on every 10 min
```
python retrainer_checker_kf.py
```

## Learn more
You can learn more about this project by reading the following [medium article]()



# Author
For any questions and/or comments, please contact **marcgeremie@gmail.com**
