import argparse
import os

from utils import utils

def check_job_relevancy(model_name, model_folder, output_dest):
    current_version = utils.get_model_current_version(model_name)
    user_name = os.environ['USER_NAME']
    pwd = os.environ['PWD']

    # Make sure the 'output_dest' path directory exists
    output_dir = os.path.dirname(output_dest)
    os.makedirs(output_dir, exist_ok=True)

    if current_version is None:
        with open(output_dest, 'w') as f:
            f.write('relevant')
    else:
        candidate_version_metric = utils.get_version_metric(user_name, pwd, 'version_{}'.format(model_folder))
        current_version_metric = utils.get_version_metric(user_name, pwd, current_version)
        if current_version_metric > candidate_version_metric:
            with open(output_dest, 'w') as f:
                f.write('relevant')
        else:
            with open(output_dest, 'w') as f:
                f.write('irrelevant')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--project_id', required=True)
    parser.add_argument('--model_name', required=True)
    parser.add_argument('--model_folder', required=True)
    parser.add_argument('--output_dest', required=True)
    args = parser.parse_args().__dict__
    
    model_name = args['model_name']
    model_folder = args['model_folder']
    output_dest = args['output_dest']

    check_job_relevancy(model_name, model_folder, output_dest)